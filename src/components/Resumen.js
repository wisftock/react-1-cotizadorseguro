import React from 'react';
import styled from '@emotion/styled';
import PropTypes from 'prop-types';

const ContenedorResumen = styled.div`
  text-align: center;
  background-color: #efefef;
  padding: 5px;
  margin-top: 10px;
`;
const Li = styled.li`
  text-transform: capitalize;
  color: #1a508b;
`;
const H3 = styled.h3`
  color: #f58634;
`;
const H2 = styled.h2`
  color: #61b15a;
`;
const Resumen = ({ datos, cotizacion, cargando }) => {
  // console.log(cargando);
  if (cargando) return null;
  return (
    <ContenedorResumen>
      <H3>Resumen de Cotizacion</H3>
      <ul>
        <Li>Marca: {datos.marca}</Li>
        <Li>Año: {datos.year}</Li>
        <Li>Plan: {datos.plan}</Li>
      </ul>
      <H2>${cotizacion}</H2>
    </ContenedorResumen>
  );
};
Resumen.propTypes = {
  datos: PropTypes.object.isRequired,
  cotizacion: PropTypes.number.isRequired,
  cargando: PropTypes.bool.isRequired,
};
export default Resumen;
