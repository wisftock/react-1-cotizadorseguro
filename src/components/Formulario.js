import React, { useState } from 'react';
import styled from '@emotion/styled';
import PropTypes from 'prop-types';
import getDiferencia, {
  calcularMarca,
  obtenerPlan,
} from '../helpers/getDiferencia';

const Campo = styled.div`
  display: flex;
  margin-bottom: 1rem;
  align-items: center;
`;
const Label = styled.label`
  flex: 0 0 100px;
`;
const Select = styled.select`
  display: block;
  width: 100%;
  padding: 5px;
  border: 1px solid #e1e1e1;
  border-radius: 5px;
  -webkit-appearance: none;
`;
const InputRadio = styled.input`
  margin: 0 1rem;
`;
const Button = styled.button`
  background-color: #55b2ed;
  border: none;
  width: 100%;
  padding: 0.5rem;
  border-radius: 5px;
  color: #fff;
  transition: all 0.3s;
  cursor: pointer;
  text-transform: uppercase;
  &:hover {
    background-color: #1e9eed;
  }
`;
const Error = styled.div`
  background-color: #ec4646;
  color: #fff;
  padding: 5px;
  text-align: center;
  margin-bottom: 10px;
`;
const Formulario = ({ setResumen, setCargando }) => {
  const initialState = {
    marca: '',
    year: '',
    plan: '',
  };
  const [datos, setDatos] = useState(initialState);
  const [error, setError] = useState(false);
  //   extraer los valores del state
  const { marca, year, plan } = datos;
  const handleOnChange = (e) => {
    setDatos({ ...datos, [e.target.name]: e.target.value });
  };
  const handleSubmit = (e) => {
    e.preventDefault();

    if (marca.trim() === '' || year.trim() === '' || plan.trim() === '') {
      setError(true);
      return;
    }
    setError(false);
    let resultado = 2000;
    // diferencia de año
    const diferencia = getDiferencia(year);
    // por cada año hay que restar el 3%
    resultado -= (diferencia * 3 * resultado) / 100;
    resultado = calcularMarca(marca) * resultado;
    const incrementoPlan = obtenerPlan(plan);
    resultado = parseFloat(incrementoPlan * resultado).toFixed(2);
    setCargando(true);
    setTimeout(() => {
      setCargando(false);
      setResumen({
        cotizacion: resultado,
        datos,
      });
    }, 2000);
  };
  return (
    <form onSubmit={handleSubmit}>
      {error && <Error>Todos los campos son obligatorios</Error>}
      <Campo>
        <Label>Marca: </Label>
        <Select name='marca' value={marca} onChange={handleOnChange}>
          <option value=''>-- Seleccione --</option>
          <option value='americano'>americano</option>
          <option value='europeo'>europeo</option>
          <option value='asiatico'>asiatico</option>
        </Select>
      </Campo>
      <Campo>
        <Label>Año: </Label>
        <Select name='year' value={year} onChange={handleOnChange}>
          <option value=''>-- Seleccione --</option>
          <option value='2021'>2021</option>
          <option value='2020'>2020</option>
          <option value='2019'>2019</option>
          <option value='2018'>2018</option>
          <option value='2017'>2017</option>
          <option value='2016'>2016</option>
          <option value='2015'>2015</option>
          <option value='2014'>2014</option>
          <option value='2013'>2013</option>
          <option value='2012'>2012</option>
        </Select>
      </Campo>
      <Campo>
        <Label>Plan: </Label>
        <InputRadio
          type='radio'
          name='plan'
          value='basico'
          checked={plan === 'basico'}
          onChange={handleOnChange}
        />
        Básico
        <InputRadio
          type='radio'
          name='plan'
          value='completo'
          checked={plan === 'completo'}
          onChange={handleOnChange}
        />
        Completo
      </Campo>
      <Button type='submit'>Cotizar</Button>
    </form>
  );
};
Formulario.propTypes = {
  setResumen: PropTypes.func.isRequired,
  setCargando: PropTypes.func.isRequired,
};
export default Formulario;
