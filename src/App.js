import React, { useState } from 'react';
import Header from './components/Header';

import styled from '@emotion/styled';
import Formulario from './components/Formulario';
import Resumen from './components/Resumen';
import Spinner from './components/Spinner';

const Contenedor = styled.div`
  max-width: 600px;
  margin: 20px auto;
`;

const ContenedorFormulario = styled.div`
  background-color: #fff;
  padding: 3rem;
`;

const App = () => {
  const [resumen, setResumen] = useState({});
  const { datos, cotizacion } = resumen;
  const [cargando, setCargando] = useState(false);
  return (
    <Contenedor>
      <Header titulo='Cotizador de seguros' />
      <ContenedorFormulario>
        <Formulario setResumen={setResumen} setCargando={setCargando} />
        {cargando && <Spinner />}
        {datos && (
          <Resumen datos={datos} cotizacion={cotizacion} cargando={cargando} />
        )}
      </ContenedorFormulario>
    </Contenedor>
  );
};

export default App;
